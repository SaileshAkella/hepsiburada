#!/bin/bash

#Listing resources required for logging
hname=${HOSTNAME%%.*}
tstamp="$( date +%Y%m%d_%H%M%S)"
print_time="$( date '+%Y-%m-%d %H:%M:%S,%3N')"
dname="$( date +%Y%m%d)"
#log_loc="/var/log"
log_loc="/usr/local/bin/flattener_service/app_src/logs"

#Activating the Falcon Virtual Environment Session
cd /usr/local/bin
source /usr/local/bin/flattener_service/flattener_py_env/bin/activate
echo Falcon Framework Environment Activated

#Shutting the Falcon Gunicorn Services
killall gunicorn

#Print the Logs to Stop Services
echo $print_time INFO: Falcon Services Shutdown >> $log_loc/Falcon_$hname"_test_"$dname".log"

echo All Services are Stopped

#Deactivating the Virtual Environment Session
deactivate
echo Flacon Framework Environment Deactivated

