#!/bin/bash

#Listing resources required for logging
hname=${HOSTNAME%%.*}
#echo $HOSTNAME
tstamp="$( date +%Y%m%d-%H%M%S)"
print_time="$( date '+%Y-%m-%d %H:%M:%S,%3N')"
dname="$( date +%Y%m%d)"
#log_loc="/var/log"
log_loc="/usr/local/bin/flattener_service/app_src/logs"

#Identifying the deployment port based on machine Hostname
getdata_srv_port=8000

#Activating the Falcon Virtual Environment Session
cd /usr/local/bin
source /usr/local/bin/flattener_service/flattener_py_env/bin/activate
echo Falcon Framework Environment Activated

#Print the Logs to Start Services
echo $print_time INFO: Falcon Services Initiated on port : $getdata_srv_port >> $log_loc/Falcon_$hname"_test_"$dname".log"

#Starting the Gunicorn Services to enable end points
cd /usr/local/bin/flattener_service/app_src
gunicorn -b 0.0.0.0:$getdata_srv_port getdata_srv:app &

#Deactivating the Virtual Environment Session
echo Falcon Services are started and are Ready for requests
deactivate
echo Safely out of Falcon Framework Virtual Environment
