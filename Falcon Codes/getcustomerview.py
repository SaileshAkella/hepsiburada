
########################################################################################
# Code Developed by CoreCompete 
# Name: getcustomerview.py
# Version: 0.1
# Function: Host falcon service for Querying CustomerView table from RTDM
# Date: 26-10-2018
########################################################################################

import falcon
import psycopg2
import phoenixdb
import phoenixdb.cursor
import json
import logger
import socket
import postgresconnect

class getcustomerview(object):

    def on_get(self, req, resp):

        """Handles GET requests"""
        try:
            resp.status = falcon.HTTP_200  # This is the default status
            resp.body = 'Hi this is customerview and dealseeker query, I am alive'
        except Exception as ex:
            resp.body = 'Hi this is customerview and dealseeker query, I am dead'

    def on_post(self, req, resp):
        
        #logging information
        host = socket.gethostname().split('.')[0]
        logs = logger.log('customerView query',host).get()
        logs.info("Request received successfully")
        
        #Connection to Postgres
        connection = psycopg2.connect(host='postgresdb', database='postgres', user='postgre', password='tOHtaCyEjL15zh0P')
        cursor = connection.cursor()
        
        """Handles POST requests"""
        #Receive the request JSON content sent from Fusion Service
        req_json_str = req.stream.read() 
        req_json_json = json.loads(req_json_str)
        
        prodtype = req_json_json['prodtype']
        storecode = req_json_json['storecode']
        memberid = req_json_json['memberid']
        anonymousid = req_json_json['anonymousid']
        
        column_name = "s"+str(prodtype)+"_"+str(storecode)
        
        cv_columns = ['s60_6804','s119_6802','s187_6809','s197_6802','s198_6807','s202_6841','s209_6802','s210_6802','s211_6802','s213_6814','s214_6814','s215_6814','s220_6814','s223_6816','s227_6813','s235_6831','s255_6807','s258_6817','s272_6802','s274_6802','s293_6802','s297_6802','s301_6802','s308_6802','s313_6802','s314_6802','s315_6802','s316_6802','s320_6802','s367_6829','s372_6804','s376_6804','s380_6813','s383_6813','s403_6810','s404_6809','s416_6809','s417_6809','s421_6809','s427_6809','s438_6831','s440_6809','s445_6809','s447_6814','s451_6825','s467_6838','s477_6838','s482_6838','s486_6809','s489_6831','s515_6810','s529_6804','s531_6825','s534_6804','s535_6825','s537_6820','s539_6820','s543_6825','s548_6820','s550_6820','s551_6820','s557_6817','s559_6820','s561_6814','s563_6825','s579_6810','s592_6814','s595_6826','s619_6831','s621_6814','s625_6814','s629_6831','s632_6814','s727_6807','s728_6807','s736_6807','s737_6807','s743_6802','s747_6803','s767_6804','s771_6838','s787_6809','s791_6820','s810_6813','s892_6809','s895_6809','s899_6807','s913_6802','s952_6831','s955_6831','s960_6831','s962_6807','s979_6809','s997_6820','s1003_6820','s1041_6814','s1055_6809','s1067_6809','s1117_6814','s1118_6804','s1134_6813','s1138_6813','s1139_6813','s1140_6813','s1145_6832','s1146_6832','s1148_6810','s1157_6826','s1160_6838','s1237_6822','s1258_6822','s1293_6817','s1298_6817','s1299_6813','s1312_6822','s1314_6822','s1317_6839','s1318_6839','s1329_6839','s1353_6822','s1398_6809','s1405_6817','s1411_6822','s1433_6810','s1434_6810','s1435_6810','s1462_6809','s1473_6813','s1488_6809','s1492_6826','s1513_6839','s1522_6822','s1542_6813','s1547_6814','s1548_6814','s1555_6831','s1571_6826','s1577_6836','s1579_6839','s1598_6817','s1630_6810','s1664_6835','s1707_6816','s1711_6835','s1712_6802','s1714_6835','s1719_6824','s1737_6802','s1828_6831','s1841_6817','s1853_6839','s1860_6813','s1867_6817','s1879_6813','s1919_6839','s1938_6813','s1972_6838','s1984_6825','s2036_6810','s2050_6810','s2063_6838','s2068_6838','s2101_6825','s2134_6826','s2163_6838','s2167_6838','s2170_6833','s2171_6833','s2227_6836','s2321_6802','s2354_6809','s2389_6838','s2411_6822','s2441_6818','s2468_6818','s2471_6809','s2502_6809','s2503_6809','s2506_6810','s2522_6817','s2526_6817','s2531_6810','s2543_6817','s2558_6817','s2563_6817','s2577_6822','s2599_6810','s2645_6817','s2685_6838','s2696_6824','s2697_6824','s2713_6810','s2722_6826','s2731_6824','s2759_6838','s2769_6802','s2776_6817','s2778_6817','s2785_6817','s2814_6809','s2823_6809','s2829_6809','s2832_6809','s2835_6809','s2839_6809','s2843_6809','s2939_6810','s2959_6810','s2964_6802','s2967_6809','s2978_6824','s3012_6802','s3045_6818','s3081_6810','s3161_6831','s3178_6809','s3203_6808','s3204_6808','s3205_6808','s3207_6808','s3208_6808','s3210_6825','s3213_6805','s3216_6808','s3252_6818','s3258_6822','s3298_6824','s3302_6824','s3307_6824','s3308_6824','s3317_6824','s3321_6824','s3327_6817','s3346_6836','s3372_6822','s3377_6822','s3383_6805','s3396_6809','s3404_6813','s3407_6804','s3408_6814','s3411_6825','s3414_6825','s3416_6825','s3418_6825','s3419_6825','s3442_6802','s3443_6802','s3445_6802','s3463_6804','s3490_6803','s3491_6803','s3492_6803','s3494_6803','s3496_6803','s3497_6822','s3507_6818','s3509_6809','s3532_6816','s3538_6831','s3543_6831','s3545_6814','s3546_6814','s3547_6814','s3550_6807','s3553_6813','s3567_6809','s3568_6804','s3576_6817','s3579_6833','s3580_6818','s3587_6829','s3592_6825','s3593_6813','s3594_6813','s3596_6813','s3597_6813','s3598_6813','s3600_6813','s3602_6813','s3604_6804','s3611_6822','s3612_6822','s3613_6822','s3617_6822','s3618_6822','s3620_6825','s3621_6818','s80122031_6804','s80154013_6806','s80223001_6813','s80312222_6814','s80317002_6818','s80361013_6831','s80384018_6818','s80384019_6818','s80385010_6818','s80385011_6818','s80385012_6818','s80385014_6818','s80385019_6818','s80385030_6818','s80385034_6818','s80385038_6818','s80385042_6818','s80385050_6818','s80385051_6818','s80402051_6807','s80406021_6835','s80406060_6835','s80406066_6835','s80406081_6835','s80406120_6835','s80406123_6835','s80406144_6835','s80407013_6804','s80416001_6809','s80428003_6807','s80428004_6807']
        
        if column_name in cv_columns:
            query = "select hbviewcnt, everpurchased,"+column_name+" from cdm.customerview where"
        else:
            query = "select hbviewcnt, everpurchased, others from cdm.customerview where"
        
        if len(memberid) == 0:
            query = query+"anonymousId = '"+anonymousid+"'"
            query2 = "select isdealseeker from cdm.dealseeker where anonmemberid ="+memberid
        else:
            query = query+"memberid = '"+memberid+"'"
            query2 = "select isdealseeker from cdm.dealseeker where anonmemberid ="+anonymousid
            
        cursor.execute(query)

        ls = cursor.fetchall()
        output = list(ls[0])
        
        if len(ls) > 0:
            temp = '{"hbviewcnt":'+str(output[0])+',"everpurchased":"'+output[1]+'","subcatcnt":'+str(output[2])+',"numofrecords":1}'
        else:
            cursor.connection.rollback()
            temp = '{"hbviewcnt":0,"everpurchased":"","subcatcnt":0,"numofrecords":1}'

        #Send the response JSON content to Fusion Service
        resp.body = temp
        resp.status = falcon.HTTP_200    

#Define the endpoint URL for Fusion Service to Send Requests
def getcustomerview_init(app):
    getcustomerview = getcustomerview()
    app.add_route('/getcustomerview', getcustomerview)

#main
# falcon.API instances are callable WSGI apps
app = falcon.API()
getcustomerview_init(app)
