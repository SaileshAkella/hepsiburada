
########################################################################################
# Code Developed by CoreCompete 
# Name: getdata.py
# Version: 0.1
# Function: Host falcon service for HBase Connection from RTDM
# Date: 29-08-2018
########################################################################################

import falcon
import psycopg2
import phoenixdb
import phoenixdb.cursor
import json
import logger
import socket
import getinformation

class getdata(object):

    def on_get(self, req, resp):

        """Handles GET requests"""
        try:
            resp.status = falcon.HTTP_200  # This is the default status
            resp.body = 'Hi this is data fetch service, I am alive'
        except Exception as ex:
            resp.body = 'Hi this is data fetch service, I am dead'

    def on_post(self, req, resp):
 
        #Logging information
        host = socket.gethostname().split('.')[0]
        logs = logger.log('Data Fetch',host).get()

        """Handles POST requests"""
        #Receive the request JSON content sent from Fusion Service
        req_json_str = req.stream.read() 
        req_json_json = json.loads(req_json_str)
        event = req_json_json['event']
        logs.info(event+" reguest received Succesfully")
        
        #Connection Setting
        if event in ['getmemberid']:
            database_url = 'http://hdpe1.hepsiburada.cloud:8765/'
            conn = phoenixdb.connect(database_url, autocommit=True)
            cursor = conn.cursor()
        
        if event in ['getcustomerview','getcontextdb','updatecustomerview','getproductviewed','getprimarysource']:
            connection = psycopg2.connect(host='postgresdb', database='postgres', user='postgre', password='tOHtaCyEjL15zh0P')
            cursor = connection.cursor()
        
        if event == 'getmemberid':
            temp = getinformation.getmemberid(cursor,req_json_json,host)
            
        if event == 'getcustomerview':
            temp, marker,status = getinformation.getcustomerview(cursor,req_json_json,host)
            
        if event == 'getcontextdb':
            temp = getinformation.getcontextdb(cursor,req_json_json,host)
            
        if event == 'updatecustomerview':
            temp = getinformation.updatecustomerview(cursor,req_json_json,host)
            
        if event == 'getproductviewed':
            temp = getinformation.getproductviewed(cursor,req_json_json,host)
            
        if event == 'getprimarysource':
            temp = getinformation.getprimarysource(cursor,req_json_json,host)
        
        #Send the response JSON content to Fusion Service
        resp.body = temp
        resp.status = falcon.HTTP_200    

#Define the endpoint URL for Fusion Service to Send Requests
def getdata_init(app):
    getdata_srv = getdata()
    app.add_route('/getdata', getdata_srv)

#main
# falcon.API instances are callable WSGI apps
app = falcon.API()
getdata_init(app)
