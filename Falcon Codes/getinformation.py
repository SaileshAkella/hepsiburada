
########################################################################################
# Code Developed by CoreCompete 
# Name: getmemberid.py
# Version: 0.1
# Function: Host falcon service for HBase Connection from RTDM
# Date: 23-08-2018
########################################################################################

import falcon
import psycopg2
import phoenixdb
import phoenixdb.cursor
import json
import logger
import socket
import colnames

def getmemberid(cursor,payload,host):
    anonid = payload['anonid']
    try:
        
        cursor.execute("select \"customer\".\"userId\" from \"hp_personalization\" where AnonymousId = '" + anonid + "'")
        ls = cursor.fetchall()
        if len(ls) > 0:
            temp = '{"memberId":"'+str(ls[0][0].replace('-',''))+'"}'
        else:
            temp = '{"memberId":""}'
            
    except:
        temp = '{"memberId":""}'
        logs = logger.log('getMemberId',host).get()
        logs.error("Query to anonymousId memberId mapping table in Hbase failed")

    finally:
        cursor.connection.close()
        return temp

def getcustomerview(cursor,payload,host):
    
    prodtype =payload['prodtype']
    storecode =payload['storecode']
    memberid =payload['memberid']
    anonymousid =payload['anonid']
    column_name = "s"+str(prodtype)[0:len(str(prodtype))-2]+"_"+str(storecode)[0:len(str(storecode))-2]
    marker = ""
    data_status = 1
    
    cv_columns = colnames.getcustview()

    if column_name in cv_columns:
        query = "select hbviewcount, everpurchased,"+column_name+" from cdm.customerview where "
    else:
        marker = "others"
        query = "select hbviewcount, everpurchased, others from cdm.customerview where "

    if len(memberid) == 0:
        query = query+"anonymousid = '"+anonymousid+"'"
        query2 = "select anonymous_member_id from cdm.dealseeker where anonymous_member_id ='"+anonymousid+"'"
    else:
        query = query+"memberid = '"+memberid+"'"
        query2 = "select anonymous_member_id from cdm.dealseeker where anonymous_member_id ='"+memberid+"'"
    
    try:
        cursor.execute(query)
        ls = cursor.fetchall()

        if len(ls) > 0:
            output = list(ls[0])
            temp = '{"hbviewcnt":'+str(output[0])+',"everpurchased":"'+output[1]+'","subcatcnt":'+str(output[2])+',"numofrecords":1,'
        else:
            data_status = 0
            temp = '{"hbviewcnt":0,"everpurchased":"","subcatcnt":0,"numofrecords":0,'
            
        cursor.execute(query2)  
        dealseeker = cursor.fetchall()

        if len(dealseeker)>0:
            temp = temp + '"isdealseeker":1}'
        else:
            temp = temp + '"isdealseeker":0}'
            
    except:
        cursor.connection.rollback()
        data_status = 0
        temp = '{"hbviewcnt":0,"everpurchased":"","subcatcnt":0,"numofrecords":0,"isdealseeker":0}'
        marker = "error"
        logs = logger.log('getCustomerView',host).get()
        logs.error("Query to customerView or dealSeeker table in PostGres failed")
        
    finally:
        if payload['event'] == 'getcustomerview':
            cursor.connection.close()
        return temp, marker, data_status

def getcontextdb(cursor,payload,host):
    
    memberid = payload['memberid']
    query = "select * from cdm.contextdb where memberid = '"+memberid+"'"
    
    try:
        cursor.execute(query)  
        ls = cursor.fetchall()

        if len(ls) > 0:

            lst = list(ls[0])
            clm_names = [desc[0] for desc in cursor.description]
            temp = '{"memberid":"'+memberid+'"'
            x = 1
            while x < len(lst):
                temp += ',"'+clm_names[x]+'":'+str(lst[x])
                x+=1
            temp = temp + '}'
        else:
            temp = '{"memberid":"'+memberid+'"'
            clm_names = colnames.getcontext()

            x = 1
            while x < len(clm_names):
                temp += ',"'+clm_names[x]+'":1'
                x+=1
            temp = temp+'}'
            logs = logger.log('getContextDB',host).get()
            logs.info("Contextdb doesnt contain data for this memberid")

    except:
        cursor.connection.rollback()
        temp = '{"memberid":"'+memberid+'"'
        clm_names = colnames.getcontext()

        x = 1
        while x < len(clm_names):
            temp += ',"'+clm_names[x]+'":1'
            x+=1
        temp = temp+'}'
        logs = logger.log('getContextDB',host).get()
        logs.error("Query to contextdb table in PostGres failed")
        
    finally:
        cursor.connection.close()
        return temp

    
def updatecustomerview(cursor,payload,host):
    
    temp, marker,data_status = getcustomerview(cursor,payload,host)
    temp_js = json.loads(temp)
    
    prodtype =payload['prodtype']
    storecode =payload['storecode']
    memberid =payload['memberid'].upper().replace('-','')
    anonymousid =payload['anonid'].upper().replace('-','')
    column_name = "s"+str(prodtype)[0:len(str(prodtype))-2]+"_"+str(storecode)[0:len(str(storecode))-2]
    
    if data_status != 0:
    
        if marker == 'others':
            query = 'UPDATE cdm.customerview SET hbviewcount='+str(temp_js['hbviewcnt']+1)+', others='+str(temp_js['subcatcnt']+1)+' WHERE '
        else:
            query = 'UPDATE cdm.customerview SET hbviewcount='+str(temp_js['hbviewcnt']+1)+', '+column_name+'='+str(temp_js['subcatcnt']+1)+' WHERE '


        if len(memberid) == 0:
            query = query+"anonymousid='"+anonymousid+"'"
        else:
            query = query+"memberid='"+memberid+"'"
    
        try:
            cursor.execute(query)
            cursor.connection.commit()
            temp = '{"status":"success"}'

        except:
            cursor.connection.rollback()
            temp = '{"status":"failed"}'
            logs = logger.log('updateCustomerView',host).get()
            logs.error("Query to update customerView table in PostGres failed")

        finally:
            cursor.connection.close()
            return temp
    else:
        temp = '{"status":"failed"}'
        logs = logger.log('updateCustomerView',host).get()
        logs.error("The memberid/anonymousid "+memberid+"/"+anonymousid+" does not exist in  customerView table")
        return temp
    
def getproductviewed(cursor,payload,host):
    
    memberid =payload['memberid']
    anonymousid =payload['anonid']
    
    query = 'select max(score) from cdm.productsviewed where '
    
    if len(memberid) == 0:
        query = query + "anonymousId = '"+anonymousid+"' and memberId is null"
    else:
        query = query + "anonymousId = '"+anonymousid+"' and memberId = '"+memberid+"'"
        
    try:
        cursor.execute(query)
        ls = cursor.fetchall()

        if len(ls) > 0:
            if t[0][0] != None:
                temp = '{"score":'+str(ls[0][0] + 1)+'}'
            else:
                temp = '{"score":1}'
        else:
            temp = '{"score":1}'
        
    except:
        cursor.connection.rollback()
        temp = '{"score":0}'
        logs = logger.log('getProductViewed',host).get()
        logs.error("Query to get productViewed table score from PostGres failed")
        
    finally:
        cursor.connection.close()
        return temp

def getprimarysource(cursor,payload,host):
    
    transactionlist = payload['transactionlist']
    primarysource = payload['primarysource']
    
    if len(transactionlist) > 0:
        query = "select b_productid, sku, score, b_store_code, b_prod_type from cdm."+primarysource+" where a_productid in "+str(tuple(transactionlist))
        
        try:
            cursor.execute(query)
            ls = cursor.fetchall()
            
            if len(ls) > 0:
                b_productid = []
                sku = []
                score = []
                b_store_code = []
                b_prod_type = []
                
                for elements in ls:
                    b_productid += [str(elements[0])]
                    sku += [str(elements[1])]
                    score += [elements[2]]
                    b_store_code += [elements[3]]
                    b_prod_type += [elements[4]]
                    
                temp = [x for _,x in sorted(zip(score,b_productid))]
                b_productid = temp[::-1]

                temp = [x for _,x in sorted(zip(score,sku))]
                sku = temp[::-1]

                temp = [x for _,x in sorted(zip(score,b_store_code))]
                b_store_code = temp[::-1]

                temp = [x for _,x in sorted(zip(score,b_prod_type))]
                b_prod_type = temp[::-1]

                score.sort()
                temp = score[::-1]
                score = temp
                
                temp = '{"b_productid":'+b_productid[0:51]+',"sku":'+sku[0:51]+',"score":'+score[0:51]+',"b_store_code":'+b_store_code[0:51]+',"b_prod_type":'+b_prod_type[0:51]+'}'
                
            else:
                temp = '{"b_productid":[],"sku":[],"score":[],"b_store_code":[],"b_prod_type":[]}'
                
        except:
            cursor.connection.rollback()
            temp = '{"b_productid":[],"sku":[],"score":[],"b_store_code":[],"b_prod_type":[]}'
            logs = logger.log('getprimarysource',host).get()
            logs.error("Query to get primarySource table from PostGres failed")
     
    else:
        temp = '{"b_productid":[],"sku":[],"score":[],"b_store_code":[],"b_prod_type":[]}'

    return temp






