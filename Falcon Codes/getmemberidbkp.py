
########################################################################################
# Code Developed by CoreCompete 
# Name: getmemberid.py
# Version: 0.1
# Function: Host falcon service for HBase Connection from RTDM
# Date: 23-08-2018
########################################################################################

import falcon
import phoenixdb
import phoenixdb.cursor
import json
import logger
import socket

class getmemberid(object):

    def on_get(self, req, resp):

        """Handles GET requests"""
        try:
            resp.status = falcon.HTTP_200  # This is the default status
            resp.body = 'Hi this is anonymousid and memberid mapper, I am alive'
        except Exception as ex:
            resp.body = 'Hi this is anonymousid and memberid mapper, I am dead'

    def on_post(self, req, resp):
        
        
        #Logging information
        host = socket.gethostname().split('.')[0]
        logs = logger.log('AnonMemberId -',host).get()
        logs.info("Reguest received Succesfully")
        
        
        """Handles POST requests"""
        #Receive the request JSON content sent from Fusion Service
        req_json_str = req.stream.read() 
        req_json_json = json.loads(req_json_str)
        anonid = req_json_json['anonid']

        database_url = 'http://hdpe1.hepsiburada.cloud:8765/'
        conn = phoenixdb.connect(database_url, autocommit=True)
        cursor = conn.cursor()
        cursor.execute("select customer_id from Hb_customer_base_tbl where anonymousid = '" + anonid + "'")
        ls = cursor.fetchall()
        if len(ls) > 0:
            temp = '{"memberId":"'+str(ls[0][0].replace('-',''))+'"}'
        else:
            temp = '{"memberId":""}'

        #Send the response JSON content to Fusion Service
        resp.body = temp
        resp.status = falcon.HTTP_200    

#Define the endpoint URL for Fusion Service to Send Requests
def getmemberid_init(app):
    getmemberid = getmemberid()
    app.add_route('/getmemberid', getmemberid)

#main
# falcon.API instances are callable WSGI apps
app = falcon.API()
getmemberid_init(app)
