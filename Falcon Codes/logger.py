import os
import logging
import datetime
import time 

class log(object):

    def __init__(self,name,host):
        logging_dir = '/usr/local/bin/flattener_service/app_src/logs'
        dt = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d')
        name = name.replace('.log','')
        logger = logging.getLogger(" "+name+" - ")    # log_namespace can be replaced with your namespace 
        logger.setLevel(logging.DEBUG)
        if not logger.handlers:
            file_name = os.path.join(logging_dir, 'Falcon_%s_test_%s.log' %(host,dt))    # usually I keep the LOGGING_DIR defined in some global settings file
            handler = logging.FileHandler(file_name)
            formatter = logging.Formatter('%(asctime)s %(levelname)s:%(name)s %(message)s')
            handler.setFormatter(formatter)
            handler.setLevel(logging.DEBUG)
            logger.addHandler(handler)
        self._logger = logger

    def get(self):
        return self._logger