
########################################################################################
# Code Developed by CoreCompete 
# Name: reco_header.py
# Version: 0.1
# Function: Host falcon service for HBase Connection from RTDM
# Date: 23-08-2018
########################################################################################

import falcon
import locale
import json
import logger
import socket

class reco_header(object):

    def on_get(self, req, resp):

        """Handles GET requests"""
        try:
            resp.status = falcon.HTTP_200  # This is the default status
            resp.body = 'Hi this is reco_header portal, I am alive'
        except Exception as ex:
            resp.body = 'Hi this is reco_header portal, I am dead'

    def on_post(self, req, resp):

        """Handles POST requests"""
        #Receive the request JSON content sent from Fusion Service
        req_json_str = req.stream.read()
        req_json_json = json.loads(req_json_str)
        placementid = req_json_json['placementId']
        host = socket.gethostname().split('.')[0]
        logs = logger.log('Reco_Header',host).get()
        logs.info("Connection Made Succesfully")
        
        RecoHeaderList = []
        for terms in placementid:
            if terms == "item_page.web-rank1":
                RecoHeaderList += [str("Bunu Alanlar Bunu da Aldi")]
            elif terms == "item_page.web-rank2":
                RecoHeaderList += ["Son Gezdiğiniz Ürünler"]
            elif terms == "item_page.web-rank3":
                RecoHeaderList += ["Aramalarınızdan Esinlendik"]
            elif terms == "item_page.web-rank4":
                RecoHeaderList += ["Bunu Alanlar Bunu da Aldı"]
            elif terms == "item_page.web-rank5":
                RecoHeaderList += ["Son Gezdiğiniz Ürünler"]
        temp = '{"RecoHeaderList":'+json.dumps(RecoHeaderList)+'}'

        #Send the response JSON content to Fusion Service
        resp.body = temp
        resp.status = falcon.HTTP_200    

#Define the endpoint URL for Fusion Service to Send Requests
def reco_header_init(app):
    reco_header_srv = reco_header()
    app.add_route('/recoheader', reco_header_srv)

#main
# falcon.API instances are callable WSGI apps
app = falcon.API()
reco_header_init(app)
