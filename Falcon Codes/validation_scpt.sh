#!/bin/bash

#Listing resources required for logging
hname=${HOSTNAME%%.*}
#echo $HOSTNAME
tstamp="$( date +%Y%m%d-%H%M%S)"
print_time="$( date '+%Y-%m-%d %H:%M:%S,%3N')"
dname="$( date +%Y%m%d)"
log_loc="/usr/local/bin/flattener_service/app_src/logs"

echo $print_time INFO: Validating Falcon Startup Scripts >> $log_loc/Falcon_$hname"_test_"$dname".log"

#Validating Falcon + Phoenix Services
echo '{"anonid":"80b6fb4e-f972-4a6b-89e4-c7004a0d401a"}' > /usr/local/bin/flattener_service/app_src/test/input.json
curl -X POST --data @/usr/local/bin/flattener_service/app_src/test/input.json http://localhost:8000/connecthbase > $log_loc/phoenix_$dname".log"
rm -rf /usr/local/bin/flattener_service/app_src/test/input.json

new_log=$log_loc/phoenix_$dname".log"
valid_log=$log_loc/phoenix_validate.log

if((`stat -c%s "$new_log"`==`stat -c%s "$valid_log"`));then
    echo $print_time INFO: Phoenix Services Validation Successful >> $log_loc/Falcon_$hname"_test_"$dname".log"
    rm -rf $log_loc/phoenix_$dname".log"
else
    echo $print_time ERROR: Phoenix Services Validation Failed >> $log_loc/Falcon_$hname"_test_"$dname".log"
fi

#Validating Reco_Header Services
echo '{"placementId":["item_page.web-rank1","item_page.web-rank2","item_page.web-rank3","item_page.web-rank4","item_page.web-rank5"]}' > /usr/local/bin/flattener_service/app_src/test/reco.json
curl -X POST --data @/usr/local/bin/flattener_service/app_src/test/reco.json http://localhost:8010/recoheader > $log_loc/reco_$dname".log"
rm -rf /usr/local/bin/flattener_service/app_src/test/reco.json

new_log=$log_loc/reco_$dname".log"
valid_log=$log_loc/reco_validate.log

if((`stat -c%s "$new_log"`==`stat -c%s "$valid_log"`));then
    echo $print_time INFO: Reco Header Services Validation Successful >> $log_loc/Falcon_$hname"_test_"$dname".log"
    rm -rf $log_loc/reco_$dname".log"
else
    echo $print_time ERROR: Reco Header Services Validation Failed >> $log_loc/Falcon_$hname"_test_"$dname".log"
fi

#Validating RTDM API Services
echo '{"version": 1,"clientTimeZone": "GMT","correlationId": "21897392735","inputs": {"AnonymousId":"80b6fb4e-f972-4a6b-89e4-c7004a0d401a","ProductId":"HB000008D916","PlacementIdList":["item_page.web-rank1","item_page.web-rank2","item_page.web-rank3","item_page.web-rank4","item_page.web-rank5"],"CustomerId":""}}' > /usr/local/bin/flattener_service/app_src/test/flowinput.json
curl -H "Content-Type:application/json" -X POST --data @/usr/local/bin/flattener_service/app_src/test/flowinput.json http://saseng1.hepsiburada.cloud:8680/RTDM/rest/decisions/hb_uc_1_event > $log_loc/rtdm_$dname".log"
rm -rf /usr/local/bin/flattener_service/app_src/test/flowinput.json

new_log=$log_loc/rtdm_$dname".log"
valid_log=$log_loc/rtdm_validate.log

if((`stat -c%s "$new_log"`==`stat -c%s "$valid_log"`));then
    echo $print_time INFO: RTDM API Services Validation Successful >> $log_loc/Falcon_$hname"_test_"$dname".log"
    rm -rf $log_loc/rtdm_$dname".log"
else
    echo $print_time ERROR: RTDM API Services Validation Failed >> $log_loc/Falcon_$hname"_test_"$dname".log"
fi

echo $print_time INFO: Validating Scripts End >> $log_loc/Falcon_$hname"_test_"$dname".log"
