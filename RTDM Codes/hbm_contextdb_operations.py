#input
#memberid
#b_prod_type
#b_store_code
#b_prodidlist
#b_skulist
#b_scorelist
#source
#b_subcatscore

import urllib2
import json

pl = '{"memberid":"'+memberid+'","event":"getcontextdb"}'

headers = {'Accept': 'application/json','Content-Type':'application/json'}
url = 'http://localhost:8000/getdata'
req = urllib2.Request(url, pl, headers)
resp = urllib2.urlopen(req)
ret = json.loads(resp.read())

columns = list(ret.keys())

temp_score = []

if source == 1:
    #primarySource contextdb operations
    x = 0
    while x < len(b_prodidlist):
        col_name = "s"+str(b_prod_type[x])[0:len(str(b_prod_type[x]))-2]+"_"+str(b_store_code[x])[0:len(str(b_store_code[x]))-2]
        
        if col_name in columns:
            temp_score += [b_scorelist[x]*ret[col_name]]
        else:
            temp_score += [b_scorelist[x]*ret['other']]
        x+=1
    outputscorelist = temp_score

elif source == 2:
    #secondarySource contextdb operations
    x = 0
    while x < len(b_prodidlist):
        col_name = "s"+str(b_prod_type[x])[0:len(str(b_prod_type[x]))-2]+"_"+str(b_store_code[x])[0:len(str(b_store_code[x]))-2]
        if col_name in columns:
            temp_score += [b_scorelist[x]*b_subcatscore[x]*ret[col_name]]
        else:
            temp_score += [b_scorelist[x]*b_subcatscore[x]*ret['other']]
        x+=1
        
    outputscorelist = temp_score

outputprodidlist = b_prodidlist
outputskulist = b_skulist
outputproductcount = len(b_skulist)
#output
#outputprodidlist
#outputskulist
#outputscorelist
#outputproductcount