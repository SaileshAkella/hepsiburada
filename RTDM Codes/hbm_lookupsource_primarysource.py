#input
#t_prodidlist
#primarysource

import urllib2
import json


if len(t_prodidlist) > 0:
    pl = '{"transactionlist":"'+t_prodidlist+'","primarysource":"'+primarysource+'","event":"getprimarysource"}'

    headers = {'Accept': 'application/json','Content-Type':'application/json'}
    url = 'http://localhost:8000/getdata'
    req = urllib2.Request(url, pl, headers)
    resp = urllib2.urlopen(req)
    ret = json.loads(resp.read())
    
    b_productid = ret['b_productid']
    sku = ret['sku']
    score = ret['score']
    b_store_code = ret['b_store_code']
    b_prod_type = ret['b_prod_type']
    
else:
    b_productid=[]
    sku=[]
    score=[]
    b_store_code=[]
    b_prod_type=[]
    
#output
#b_productid
#sku
#score
#b_store_code
#b_prod_type