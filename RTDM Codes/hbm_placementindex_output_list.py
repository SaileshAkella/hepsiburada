#input
#p1_productidlist
#p1_skulist
#p1_scorelist
#p1_scenarioid
#p2_p3_productidlist
#p2_p3_skulist
#p2_p3_scorelist
#p2_p3_scenarioid
#otherPlacementIndex

if otherPlacementIndex == 2:
    outputprodidlist = p2_p3_productidlist
    outputskulist = p2_p3_skulist
    outputscorelist = p2_p3_scorelist
    outputscenarioid = p2_p3_scenarioid
    
elif otherPlacementIndex == 3:
    outputprodidlist = list(p1_productidlist)+list(p2_p3_productidlist)
    outputskulist = list(p1_skulist)+list(p2_p3_skulist)
    outputscorelist = list(p1_scorelist)+list(p2_p3_scorelist)
    outputscenarioid = p2_p3_scenarioid
            
#output
#outputprodidlist
#outputskulist
#outputscorelist
#outputscenarioid