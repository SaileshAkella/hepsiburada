#input
#b_productidlist
#b_skulist
#b_scorelist
#b_prodtype
#b_storecode
#transactionprodidlist

if len(b_skulist) > 0:
    
    temp = [x for _,x in sorted(zip(b_scorelist,b_productidlist))]
    b_productidlist = temp[::-1]

    temp = [x for _,x in sorted(zip(b_scorelist,b_skulist))]
    b_skulist = temp[::-1]

    temp = [x for _,x in sorted(zip(b_scorelist,b_prodtype))]
    b_prodtype = temp[::-1]

    temp = [x for _,x in sorted(zip(b_scorelist,b_storecode))]
    b_storecode = temp[::-1]

    b_scorelist.sort()
    temp = b_scorelist[::-1]
    b_scorelist = temp

    temp_b_skulist = []
    temp_b_productidlist = []
    temp_b_scorelist = []
    temp_b_prodtype = []
    temp_b_storecode = []
    
    x=0
    while x < len(b_skulist):
        sku = b_skulist[x]
        if sku not in temp_b_skulist:
            temp_b_skulist += [b_skulist[x]]
            temp_b_productidlist += [b_productidlist[x]]
            temp_b_scorelist += [b_scorelist[x]]
            temp_b_prodtype += [b_prodtype[x]]
            temp_b_storecode += [b_storecode[x]]
        x+=1
            
    b_skulist = temp_b_skulist 
    b_productidlist = temp_b_productidlist
    b_scorelist = temp_b_scorelist
    b_prodtype = temp_b_prodtype
    b_storecode = temp_b_storecode
    
if len(transactionprodidlist) > 0:
    temp_prodid = []
    temp_sku = []
    temp_score = []
    temp_prodtype = []
    temp_storecode = []
    
    x=0
    while x < len(b_productidlist):
        prodid = b_productidlist[x]
        if prodid not in transactionprodidlist:
            temp_prodid += [b_productidlist[x]]
            temp_sku += [b_skulist[x]]
            temp_score += [b_scorelist[x]]
            temp_prodtype += [b_prodtype[x]]
            temp_storecode += [b_storecode[x]]
        x+=1
   
    outputprodidlist = temp_prodid
    outputskulist = temp_sku
    outputscorelist = temp_score
    outputprodtype = temp_prodtype
    outputstorecode = temp_storecode
    
else:
    outputprodidlist = b_productidlist
    outputskulist = b_skulist
    outputscorelist = b_scorelist
    outputprodtype = b_prodtype
    outputstorecode = b_storecode

outputproductcount = len(outputprodidlist)
    
#output
#outputprodidlist
#outputskulist
#outputscorelist
#outputprodtype
#outputstorecode
#outputproductcount