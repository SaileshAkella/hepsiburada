#input
#unsortedprodidlist
#unsortedskulist
#unsortedscorelist


temp = [x for _,x in sorted(zip(unsortedscorelist,unsortedprodidlist))]
sortedprodidlist = temp[::-1]

temp = [x for _,x in sorted(zip(unsortedscorelist,unsortedskulist))]
sortedskulist = temp[::-1]

unsortedscorelist.sort()
sortedscorelist = unsortedscorelist[::-1]

if len(sortedprodidlist) > 25:
    sortedprodidlist = sortedprodidlist[0:25]
    sortedskulist = sortedskulist[0:25]
    sortedscorelist = sortedscorelist[0:25]
    
sortedproductscount = len(sortedprodidlist)

#output
#sortedprodidlist
#sortedskulist
#sortedscorelist
#sortedproductscount