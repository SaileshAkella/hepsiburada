import urllib2
import json

if len(anonymousid) > 0:
    anonymousid = anonymousid.replace('-','')

if memberid == None:
    pl = '{"anonid":"'+anonymousid+'","memberid":"","prodtype":'+str(prodtype)+',"storecode":'+str(storecode)+',"event":"getcustomerview"}'
    
elif len(memberid) == 0:
    pl = '{"anonid":"'+anonymousid+'","memberid":"","prodtype":'+str(prodtype)+',"storecode":'+str(storecode)+',"event":"getcustomerview"}'
else:
    pl = '{"anonid":"'+anonymousid+'","memberid":"'+memberid+'","prodtype":'+str(prodtype)+',"storecode":'+str(storecode)+',"event":"getcustomerview"}'
    
headers = {'Accept': 'application/json','Content-Type':'application/json'}
url = 'http://localhost:8000/getdata'
req = urllib2.Request(url, pl, headers)
resp = urllib2.urlopen(req)
ret = json.loads(resp.read())
print(ret)

everpurchased = ret['everpurchased'].upper()
hbviewcnt = ret['hbviewcnt']
subcatviewcnt = ret['subcatcnt']
isdealseeker = ret['isdealseeker']
numberofrecords = ret['numofrecords']