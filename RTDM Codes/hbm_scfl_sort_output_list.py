#input
#unsortedprodidlist
#unsortedskulist
#unsortedscorelist
#primaryprodcount


temp = [x for _,x in sorted(zip(unsortedscorelist,unsortedprodidlist))]
sortedprodidlist = temp[::-1]

temp = [x for _,x in sorted(zip(unsortedscorelist,unsortedskulist))]
sortedskulist = temp[::-1]

unsortedscorelist.sort()
sortedscorelist = unsortedscorelist[::-1]

num = 25 - primaryprodcount

if len(sortedprodidlist) > num:
    sortedprodidlist = sortedprodidlist[0:num]
    sortedskulist = sortedskulist[0:num]
    sortedscorelist = sortedscorelist[0:num]
    
secondaryproductcount = len(sortedprodidlist)

#output
#sortedprodidlist
#sortedskulist
#sortedscorelist
#secondaryproductcount