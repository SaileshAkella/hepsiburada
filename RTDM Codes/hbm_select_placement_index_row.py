#input
#placementindex
#placementdefidlist
#productlookupsourcelist
#primarysourcelist
#primarysourcetypelist
#sortbyprop1list
#secondarysourcelist
#sortbyprop2list
#placementdefnamelist
#placementindexlist

if len(placementindexlist) == 0:
    placementdefid = 0
    productlookupsource = ''
    primarysource = ''
    primarysourcetype = ''
    sortbyprop1 = 0
    secondarysource = ''
    sortbyprop2 = 0
    placementdefname = ''
    otherplacementindex = 0
    
elif len(placementindexlist) == 1:
    placementdefid = placementdefidlist[0]
    productlookupsource = productlookupsourcelist[0]
    primarysource = primarysourcelist[0]
    primarysourcetype = primarysourcetypelist[0]
    sortbyprop1 = sortbyprop1list[0]
    secondarysource = secondarysourcelist[0]
    sortbyprop2 = sortbyprop2list[0]
    placementdefname = placementdefnamelist[0]
    otherplacementindex = 0
    
else:
    ind = placementindexlist.index(placementindex)
    placementdefid = placementdefidlist[ind]
    productlookupsource = productlookupsourcelist[ind]
    primarysource = primarysourcelist[ind]
    primarysourcetype = primarysourcetypelist[ind]
    sortbyprop1 = sortbyprop1list[ind]
    secondarysource = secondarysourcelist[ind]
    sortbyprop2 = sortbyprop2list[ind]
    placementdefname = placementdefnamelist[ind]
    if placementindex == 1:
        x = 0
        while x < 2:
            if x != ind:
                otherplacementindex = placementindexlist[x]
            x+=1
    else:
        otherplacementindex = 0
            
#output
#placementdefid
#productlookupsource
#primarysource
#primarysourcetype
#sortbyprop1
#secondarysource
#sortbyprop2
#placementdefname
#otherplacementindex