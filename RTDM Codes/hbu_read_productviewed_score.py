import urllib2
import json

if len(anonymousid) > 0:
    anonymousid = anonymousid.replace('-','')

if memberid == None:
    pl = '{"anonid":"'+anonymousid+'","memberid":"","event":"getproductviewed"}'
    
elif len(memberid) == 0:
    pl = '{"anonid":"'+anonymousid+'","memberid":"","event":"getproductviewed"}'
else:
    pl = '{"anonid":"'+anonymousid+'","memberid":"'+memberid+'","event":"getproductviewed"}'
    
headers = {'Accept': 'application/json','Content-Type':'application/json'}
url = 'http://localhost:8000/getdata'
req = urllib2.Request(url, pl, headers)
resp = urllib2.urlopen(req)
ret = json.loads(resp.read())

score = ret['score']